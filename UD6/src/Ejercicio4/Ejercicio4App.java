package Ejercicio4;

import java.util.Scanner;

public class Ejercicio4App {

	public static void main(String[] args) {
		//pasamos el metodo
		factorial();

	}
	
	//con este metodo se quiere recuperar el factorial de un numero 
	public static void factorial() {
		 long factorial=1;
	        int num;
	        Scanner numero = new Scanner(System.in);
	        System.out.print("Introduce un n�mero: ");
	        num = numero.nextInt();
	        //Con este bucle se logra que el numero se vaya multiplicando por un numero inferior al que se introdujo hasta llegar a 0
	        for (int i = num; i > 0; i--) {
	            factorial = factorial * i;
	        }
	        System.out.println("El factorial de " + num + " es: " + factorial);
	    }
	}

