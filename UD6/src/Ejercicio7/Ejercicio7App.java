package Ejercicio7;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class Ejercicio7App {

	public static void main(String[] args) {
		moneda();

	}

	//En este metodo se se multiplica los euros que se quieren el valor de cada moneda 
	public static void moneda () {
		Scanner obtenerNumero = new Scanner(System.in);
		String moneda= JOptionPane.showInputDialog("Introduce la moneda");
		
		switch (moneda) {
		case "libra":
			double libra = 0.86;
			
			String eurosL = JOptionPane.showInputDialog("Introduce las libras que quieres pasar a euros");
			int eurosLN = Integer.parseInt(eurosL);
			
			double resultado = eurosLN * libra;
			
			JOptionPane.showMessageDialog(null,resultado + " euros");
			
			break;
			
		case "dolar":
			double dolar = 1.28611;
			
			String eurosD = JOptionPane.showInputDialog("Introduce los dolares que quieres pasar a euros");
			int eurosDN = Integer.parseInt(eurosD);
			
			double resultadoD = eurosDN * dolar;
			
			JOptionPane.showMessageDialog(null,resultadoD + " euros");
			break;
			
		case "yenes":
			double yenes = 129.852;
			
			String eurosY = JOptionPane.showInputDialog("Introduce los dolares que quieres pasar a euros");
			int eurosYN = Integer.parseInt(eurosY);
			
			double resultadoY = eurosYN * yenes;
			
			JOptionPane.showMessageDialog(null,resultadoY + " euros");
			break;
		
		default:
			JOptionPane.showMessageDialog(null,"No has introducido una moneda valida");
		}
			
		
		
	}
}
