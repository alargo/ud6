package Ejercicio6;

import java.util.Scanner;

public class EjercicioApp {

	public static void main(String[] args) {
		Scanner obtenerNumero = new Scanner(System.in);
		System.out.print("Ingresa un numero: ");
		int numero = obtenerNumero.nextInt();
		
		int resultado = cifras(numero);
		System.out.println(resultado);

	}
	
	//Para controlar el numero de cifras se divide por 10 para ir corriendo la coma y que cuente las cifras
	public static int cifras (int numero) {
		int contador = 0;
		
		while(numero != 0 ) {
			numero = numero / 10;
			contador++;
		}
		return contador;
		
	}

}
