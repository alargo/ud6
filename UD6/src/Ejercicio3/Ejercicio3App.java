package Ejercicio3;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class Ejercicio3App {

	public static void main(String[] args) {
		//depende del resultado que nos de el metodo mostrara si es primo o no el numero 
		if(primo() == true) {
			System.out.println("El numero es primo");
		}else{
			System.out.println("El numero no es primo");
		}
		
	
	}
	
	//Generamos un booleano para saber si el numero es primo o no 
	
    public static boolean primo(){
		Scanner obtenerNumero = new Scanner(System.in);
        int contador,numPrimo,numero;
 
        System.out.print("Ingresa un numero: ");
        numero = obtenerNumero.nextInt();
        contador = 0;
        for(numPrimo = 1; numPrimo <= numero; numPrimo++){
            if((numero % numPrimo) == 0){
                contador++;
            }
        }
 
        if(contador <= 2){
        	return true;
        }else{
        	return false;
        }
    }

}
	

	
	
	
