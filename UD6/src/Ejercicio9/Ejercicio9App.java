package Ejercicio9;

import java.util.Random;
import java.util.Scanner;

public class Ejercicio9App {

	public static void main(String[] args) {
		Scanner obtenerNumero = new Scanner(System.in);
		int numeros[] = new int[10];
		ponernumeros(numeros);
		sumar(numeros);

	}
	
	//Se usan los mismos metodos que en el ejercicio anterior
    private static int numeroaleatorio() {
    	Random numero = new Random();
        int numeroaleatorio = numero.nextInt(10);

        return numeroaleatorio;
    }
	
	public static void ponernumeros(int numeros[]) {
		
        Scanner entrada = new Scanner(System.in);
        for (int i = 0; i < numeros.length; i++) {
            int numero = numeroaleatorio();
            numeros[i] = numero;
        }
        mostrarnumeros(numeros);
    }

    private static void mostrarnumeros(int numeros[]) {
        System.out.println();
        System.out.println("Valores array:");
        for (int i = 0; i < numeros.length; i++) {
            System.out.println("[" +i+ "] = "+numeros[i]);
        }
    }
    

    //Se usa este metodo para tomar todos los numeros, sumarlos y mostrar la suma total
    
    private static void sumar( int numeros[] ) {
    	int total=0;
    	for (int i = 0; i < numeros.length; i++) {
    		total = total + numeros[i];
            
        }
    	System.out.println("La suma es: " + total);
    }
    
    

}
