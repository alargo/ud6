package Ejercicio10;

import java.util.Random;
import java.util.Scanner;

public class EjercicioApp10 {

	public static void main(String[] args) {
		Scanner obtenerNumero = new Scanner(System.in);
		int numeros[] = new int[10];
		ponernumeros(numeros);
	

	}
	
	//Se genera el numero aleatorio que ira dentro del array
    private static int numeroaleatorio() {
    	Random numero = new Random();
        int numeroaleatorio = numero.nextInt(100);
        

        return numeroaleatorio;
    }
	
	public static void ponernumeros(int numeros[]) {
		
        Scanner entrada = new Scanner(System.in);
        for (int i = 0; i < numeros.length; i++) {
            int numero = numeroaleatorio();
            if (primo(numero)) {
            	numeros[i] = numero;
			}else {
				i--;
			}
            
        }
        mostrarnumeros(numeros);
    }

    private static void mostrarnumeros(int numeros[]) {
        System.out.println();
        System.out.println("Valores array:");
        for (int i = 0; i < numeros.length; i++) {
            System.out.println("[" +i+ "] = "+numeros[i]);
        }
    }
    
    //Se mira que numeros son primos y que los muestre en nuestro array
    
    public static boolean primo(int numero){
        int contador,numPrimo;
        
        contador = 0;
        for(numPrimo = 1; numPrimo <= numero; numPrimo++){
            if((numero % numPrimo) == 0){
                contador++;
            }
        }
 
        if(contador <= 2){
        	return true;
        }else{
        	return false;
        }
    }

}
