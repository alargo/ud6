package Ejercicio1;

import javax.swing.JOptionPane;


public class Ejercicio1App {

	public static void main(String[] args) {
		//Primero se le pasa la figura que se desea obtener los datos 
		String figura= JOptionPane.showInputDialog("Introduce la figura");
		
		//Dentro de cada figura se pasa los datos de String a double y despues el metodo con el dato que se le paso
		switch (figura) {
		case "circulo":
			String radio = JOptionPane.showInputDialog("Introduce el radio");
			int radioN = Integer.parseInt(radio);
			
			double resultadoC = circulo(radioN);
			
			JOptionPane.showMessageDialog(null,"El area del circulo es " + resultadoC);
			break;
			
		case "triangulo":
			String base = JOptionPane.showInputDialog("Introduce la base");
			int baseN = Integer.parseInt(base);
			
			String altura = JOptionPane.showInputDialog("Introduce la altura");
			int alturaN = Integer.parseInt(altura);
			
			double resultadoT = triangulo(baseN, alturaN);
			
			JOptionPane.showMessageDialog(null,"El area del triangulo es " + resultadoT);
			break;
			
		case "cuadrado":
			String lado = JOptionPane.showInputDialog("Introduce el lado");
			int ladoN = Integer.parseInt(lado);
			
			double resultado = cuadrado(ladoN);
			
			JOptionPane.showMessageDialog(null,"El area del cuadrado es " + resultado);
			break;
		
		default:
			JOptionPane.showMessageDialog(null,"No has introducido una figura valida");
		}
			
	}
	
	//cada metodo obtiene el area de una figura diferente
	
	public static double circulo (double radio) {
		double resultado = Math.pow(radio, 2)* Math.PI;
				
		return resultado;
	}
	
	public static double triangulo (double base, double altura) {
		double resultado = (base * altura) / 2;
				
		return resultado;
	}
	
	public static double cuadrado (double lado) {
		double resultado = lado * lado;
				
		return resultado;
	}

}
