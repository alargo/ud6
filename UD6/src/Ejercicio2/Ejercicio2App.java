package Ejercicio2;

import java.util.Random;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Ejercicio2App {

	//Introducimos el numero y despues se controla el rango en el cual se quieren los numeros
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
        System.out.print("Introduce cuantos numeros quieres imprimir:");
        int aleatorio = entrada.nextInt();
        
        System.out.print("Introduce el rango inicial: ");
        int numero1 = entrada.nextInt();
        
        System.out.print("Introduce el rango final: ");
        int numero2 = entrada.nextInt();
        
        for (int i = 0; i < aleatorio; i++) {
            int numeroAleatorio = Aleatorios(numero1, numero2);
            System.out.println(numeroAleatorio);
        }


    }

	//generamos los numeros aletarios dentro del rango que se le pasa

    public static int Aleatorios(int numero1, int numero2) {
        Random numero = new Random();
        int aleatorio = numero.nextInt(numero2-numero1+1)+numero1;
        return aleatorio;
    }

}
