package Ejercicio8;

import java.util.Scanner;

public class Ejercicio8App {

	public static void main(String[] args) {
		Scanner obtenerNumero = new Scanner(System.in);
		int numeros[] = new int[10];
		ponernumeros(numeros);

	}
	//Con este metodo se  generan los numeros que van a estar en el array
	public static void ponernumeros(int numeros[]) {
		
        Scanner entrada = new Scanner(System.in);
        for (int i = 0; i < numeros.length; i++) {
            System.out.print("Introduce valor para la posicion [" +i+ "]: ");
            int numero = entrada.nextInt();
            numeros[i] = numero;
        }
        mostrarnumeros(numeros);
    }

	//Con este mostramos los numeros que hemos pasado en el array
    public static void mostrarnumeros(int numeros[]) {
        System.out.println();
        System.out.println("Valores array:");
        for (int i = 0; i < numeros.length; i++) {
            System.out.println("[" +i+ "] = "+numeros[i]);
        }
    }

}
