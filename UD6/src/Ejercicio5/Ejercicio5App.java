package Ejercicio5;

import java.util.Scanner;

public class Ejercicio5App {

	public static void main(String[] args) {

		Binario();

	}
	
	public static void Binario() {
		Scanner obtenerNumero = new Scanner(System.in);
		int decimal, modulo, aux;
		String binario = "";
		System.out.print("Ingresa un numero: ");
		decimal = obtenerNumero.nextInt();
		aux = decimal;
		
		//Se controla que el numero sea mayor a 0 y se convierte el numero 
		while(decimal > 0) {
			modulo = (decimal%2);
			binario = modulo + binario;
			decimal = decimal/2;
		}
		System.out.println("El numero decimal " + aux + " en binario es " + binario );
    }

}
