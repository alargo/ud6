package Ejercicio12;

import java.util.Random;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Ejercicio12App {

	public static void main(String[] args) {
		//Se pide el tama�o del numero que se quiere
        String texto=JOptionPane.showInputDialog("Introduce un tama�o");
        int numero[]=new int[Integer.parseInt(texto)];
 
        int ultimoNumero;
        do{
        	//Se pide un numero que sea valido
            texto=JOptionPane.showInputDialog("Introduce numero entre 0 y 9");
            ultimoNumero=Integer.parseInt(texto);
        }while(!(ultimoNumero>=0 && ultimoNumero<=9));
 

        NumAleatorio(numero, 1, 300);
 
        int terminadosEn[]=ultimoNumero(numero, ultimoNumero);
 

        numeroTerminadoEn(terminadosEn);
 
    }
	
	//En esta clase se va a generar los numeros 
    public static void NumAleatorio(int lista[], int a, int b){
        for(int i=0;i<lista.length;i++){
            lista[i]=((int)Math.floor(Math.random()*(a-b)+b));
        }
    }
  
    //Aqui vamos a almacenar el ultimo numero que se le paso anteriormente
    public static int[] ultimoNumero (int num[], int ultimoNumero){
        int terminadosEn[]=new int[num.length];
 
        int numeroF=0;
 
        for (int i=0;i<terminadosEn.length;i++){
        	numeroF=num[i]-(num[i]/10*10);
            if (numeroF==ultimoNumero){
                terminadosEn[i]=num[i];
            }
        }
 
        return terminadosEn;
    }
    
    //Aqui nos encargaremos de mostrarlo
    public static void numeroTerminadoEn(int lista[]){
        for(int i=0;i<lista.length;i++){
            if(lista[i]!=0){
                System.out.println("El numero es: " +lista[i]);
            }
        }
    }
 
}
